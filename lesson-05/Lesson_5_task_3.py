from datetime import datetime, timedelta
import argparse

parser = argparse.ArgumentParser(description='Time shift')
parser.add_argument('action', type=str, help='Choose action + or -')
parser.add_argument('number', type=int, help='Delta time')
parser.add_argument('period', type=str, help='Period of time: h (hours), d (days), m (month), y (years)')
args = parser.parse_args()

if args.action == "+":
    if args.period == "h":
        print((datetime.now() + timedelta(hours=args.number)).strftime('%Y-%m-%d'))
    elif args.period == "d":
        print((datetime.now() + timedelta(days=args.number)).strftime('%Y-%m-%d'))
    elif args.period == "m":
        print((datetime.now() + timedelta(weeks=4*args.number)).strftime('%Y-%m-%d'))
    elif args.period == "y":
        print((datetime.now() + timedelta(days=365*args.number)).strftime('%Y-%m-%d'))
elif args.action == "-":
    if args.period == "h":
        print((datetime.now() - timedelta(hours=args.number)).strftime('%Y-%m-%d'))
    elif args.period == "d":
        print((datetime.now() - timedelta(days=args.number)).strftime('%Y-%m-%d'))
    elif args.period == "m":
        print((datetime.now() - timedelta(weeks=4*args.number)).strftime('%Y-%m-%d'))
    elif args.period == "y":
        print((datetime.now() - timedelta(days=365*args.number)).strftime('%Y-%m-%d'))
else:
    print("Incorrect command")
