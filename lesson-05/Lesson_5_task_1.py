#f = open("test.txt","w")
#f.write("String 1"+'\n'+"String 2"+'\n'+"String 3"+'\n')
#f.close()
#f = open("test.txt","r")
#for line in f:
#    print(line)
#f.close()
#f = open("test.txt","rb")
#while True:
#    buff = f.read(10)
#    if not buff:
#        break
#    print(buff.decode("utf-8"))
#f.close()
f = open("test_a.txt","a")
f.writelines(["\nString_1", "\nString_2","\nString_3"])
f.close()
f = open("test_a.txt", "r")
print(f.read())
f.close()

f = open("test_b.txt","ab")
string_1 = "String_1"
f.write(string_1.encode('utf-8'))
f.close()
f = open("test_b.txt","rb")
while True:
    buff = f.read(10)
    if not buff:
        break
    print(buff.decode('utf-8'))
f.close()