from itertools import count
import sys
import re

sum_words = 0
for line in sys.stdin:
    sum_words_string = len(re.split(r'[ - ,.!? \t\r\n\(\)\{\}\[\]\|]+', line))
    sum_words = sum_words + sum_words_string
print(sum_words)