from ast import arg
from datetime import datetime, date, timedelta
import argparse

parser = argparse.ArgumentParser(description='Time shift')
parser.add_argument('date_1', type=str, help='Enter Date 1: DD.MM.YYYY')
parser.add_argument('date_2', type=str, help='Enter Date 2: DD.MM.YYYY')
parser.add_argument('period', type=str, help='Period of time between dates: h (hours), d (days), m (month), y (years)')
args = parser.parse_args()
date_1 = args.date_1.split('.')
print(date_1)
date_2 = args.date_2.split('.')
print(date_2)
date_1 = (int(date_1[0]),int(date_1[1]),int(date_1[2]))
print(date_1)
date_2 = (int(date_2[0]),int(date_2[1]),int(date_2[2]))
print(date_2)

if args.period == "y":
    result_years = date_2[2]-date_1[2]
    if result_years < 0:
        result_years *=-1
    result_years_absolut = (date_1[2]*12*30 + date_1[1]*30 + date_1[0]) - \
        (date_2[2]*12*30 + date_2[1]*30 + date_2[0])
    if result_years_absolut < 0:
        result_years_absolut *=-1
    print(f"The difference between Date 1 and Date 2 in years is: {result_years}, absolut: {result_years_absolut//365}")
elif args.period == "m":
    result_months = (date_1[2]*12+date_1[1])-(date_2[2]*12+date_2[1])
    if result_months < 0:
        result_months *=-1
    result_months_absolute = (date_1[2]*12*30 + date_1[1]*30 + date_1[0]) - \
        (date_2[2]*12*30 + date_2[1]*30 + date_2[0])
    if result_months_absolute < 0:
        result_months_absolute *=-1
    print(f"The difference between Date 1 and Date 2 in month is: {result_months}, absolute: {result_months_absolute//30}")
elif args.period == "d":
    date_11 = datetime(day=date_1[0], month=date_1[1], year=date_1[2])
    date_22 = datetime(day=date_2[0], month=date_2[1], year=date_2[2])
    countdays = date_11 - date_22
    result_days = countdays.days
    if result_days < 0:
        result_days *=-1
    print(f"The difference between Date 1 and Date 2 in days is: {result_days}")
elif args.period == "h":
    date_11 = datetime(day=date_1[0], month=date_1[1], year=date_1[2])
    date_22 = datetime(day=date_2[0], month=date_2[1], year=date_2[2])
    countdays = date_11 - date_22
    result_hours = countdays.days*24
    if result_hours < 0:
        result_hours *=-1
    print(f"The difference between Date 1 and Date 2 in hours is: {result_hours}")
else:
    print("Incorrect command")