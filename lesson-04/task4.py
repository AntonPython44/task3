#x = 3
#y = 432.21
#s = "string"
#l = [x, s]
#t = (s, y)
#print (f"{type (x)}")
#print (f"{type (x+y)}")
print ("Calculate the Hypotenuse of the triangle!")

katet_a = input ("Please, enter the leg A of the triangle:")
print (f"OK, the leg A = {katet_a}")
katet_a = float(katet_a)

katet_b = input ("Please, enter the leg B of the triangle:")
print (f"OK, the leg B = {katet_b}")
katet_b = float(katet_b)

hypotenuse = ((katet_a**2)+(katet_b**2))**(1/2)
print (f"Hypotenuse of the triangle ={hypotenuse}")
