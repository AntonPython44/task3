def fibonacci(n, res):
    if not res:
        res.append(0)
    if n == 0 or n == 1:
        if n > len(res)-2:
            res.append(n)
        return n
    x = fibonacci(n-1, res) + fibonacci(n-2, res)
    if n > len(res)-2:
        res.append(x)
    return x

fibo_numbers = []
fibonacci(10, fibo_numbers)
print(fibo_numbers)


