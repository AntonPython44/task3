def fibonacci(n):
    a, b = 0, 1
    for i in range(n):
        yield a
        a, b = b, a + b

#fibo_numbers = list(fibonacci(10))
#print(fibo_numbers)
# или
for nn in fibonacci(6):
    print(nn)